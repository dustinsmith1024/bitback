var Bitcoin = Backbone.Model.extend({});

var BitcoinView = Backbone.View.extend({
	tagName: 'div',
	className: 'bitcoin',

	render: function(){
		//this.setPrice();
		var template = $("#btc-template").html();
		this.$el.html(_.template(template, this.model.attributes));
		return this;
	},

	setPrice: function(){
		this.$el.text(this.model.get('bid'));
	}
});

var bitcoin = new Bitcoin({bid: 'Loading', ask: 'Loading', last: 'Loading'});
var bitCoinView = new BitcoinView({model: bitcoin, el: '#bitcoin'});

var ref = new Firebase("https://publicdata-bitcoin.firebaseio.com/");
ref.child("bid").on("value", setPrice);
ref.child("ask").on("value", setPrice);
ref.child("last").on("value", setPrice);

function setPrice(snapshot) {
	console.log(snapshot.name() + ": " + snapshot.val());
	bitcoin.set(snapshot.name(), snapshot.val());
	bitCoinView.render();
}


/*
_(myModels).each(function(model){
	$('#canvas').append(new RectangleView({model: model}).render().el);		
});


	var Rectangle = Backbone.Model.extend({});

	var RectangleView = Backbone.View.extend({
		tagName: 'div',
		className: 'rectangle',

		render: function(){
			this.setDimensions();
			this.setPosition();
			this.setColor();
			return this;
		},

		events: {
			'click': 'move'
		},

		setDimensions: function(){
			this.$el.css({
				width: this.model.get('width') + 'px', 
				height: this.model.get('height') + 'px'
			});
		},

		setColor: function() {
			this.$el.css('background', this.model.get('color'));
		},

		setPosition: function(){
			var position = this.model.get('position');
			this.$el.css({
				left: position.x,
				top: position.y
			});
		},

		move: function(){
			console.log('move!', this.$el.position().left);
			this.$el.css('left', this.$el.position().left + 10);
		}

	});

	var myModels = [
		new Rectangle({
		width: 100,
		height: 60,
		color: 'red',
		position: {
			x: 300,
			y: 150
		}}
		),
		new Rectangle({
		width: 40,
		height: 60,
		color: 'black',
		position: {
			x: 500,
			y: 50
		}})
	];

	_(myModels).each(function(model){
		$('#canvas').append(new RectangleView({model: model}).render().el);		
	});
	
*/


	

